//final code
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const config = require('./config');

var numb = 1;

users = [];
strangers = [];
stranger = 'dddd';

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
})


io.on('connection', function (socket) {

    console.log('a user connect with socket id=' + socket.id + "!");
    // store the room in a var for convenience
    var room = "room" + numb;
    //store two user in single room
    var user = [];
    // join socket to the next open room
    socket.join(room);

    // store room on socket obj for use later - IMPORTANT
    socket.current_room = room;
   // console.log('current room =',socket.current_room);

    // log some stuff for testing
    console.log("Joined room: ", socket.current_room, "users=", socket.id);

    // Check room occupancy, increment if 2 or more
    io.in(room).clients(function (err, clients) {
        console.log('user room no=',room);
        console.log('clients', clients);
        user.push(clients);
        console.log(clients.length);
        //   console.log(user.length);

        if (clients.length == 1) {
          //  var msg1 = "waiting for user for chat";
           // var obj1 = JSON.parse(msg1);
            console.log('current room no:',room,',waiting for user for chat and current user=',clients[0]);
            io.in(socket.current_room).emit('waiting',clients);
        } 
        if(clients.length ==2){
            console.log('current room no:',room,',connected with stranger:',clients[1]);
            io.in(socket.current_room).emit('connected',clients)
        }
        if (clients.length >= 2) {
            numb++;
        }
        console.log('----------')
    });

    /*    if (strangers.indexOf(socket.id) == -1) {
            strangers.push(socket.id);
            console.log('login strangers 1', strangers);
        }
        if (strangers[strangers.length - 1]) {
            users.push(socket.id);
            console.log('login users 2', users);
            stranger = strangers[strangers.length - 2]
            console.log('login stranger 1', stranger)
            socket.emit('loginSuccess', socket.id, stranger);
            io.sockets.emit('system', socket.id, users.length, 'login');
        } else {
            socket.emit('wait', socket.id)
        }
    };*/


    socket.on('sender', function (msg) {
        // emit to that sockets room, now we use that current_room
        console.log("msg", msg);
       
        io.in(socket.current_room).emit('chat message', msg);

    });


    socket.on('disconnect', function () {
        console.log('socket disconnected');
        console.log(room);
        var id = socket.id;
    
        console.log('deleted room no =',socket.current_room,'deleted user',id);
        console.log('dleted current room no=',socket.current_room,'remaining user in room',user[0])
        io.in(room).emit('logout');
        
    });
});

http.listen(config.port, () => {
    console.log('server is started @', config.port);
});


//demo 5
/*
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const config = require('./config');
users = [];
strangers = [];
stranger = 'dddd';


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

Array.prototype.removeByValue = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) {
            this.splice(i, 1);
            break;
        }
    }
}

var numb = 1;

io.on('connection', function (socket) {

  
    // store the room in a var for convenience
    var room = "room" + numb;
    // join socket to the next open room
    socket.join(room);

    // store room on socket obj for use later - IMPORTANT
    socket.current_room = room;
    console.log(socket.current_room);

    // log some stuff for testing
    //console.log('made socket connection', socket.id);
    console.log("Joined room: ", socket.current_room, "users=", socket.id);

    // Check room occupancy, increment if 2 or more
    io.in(room).clients(function (err, clients) {
        console.log(clients);
        if (clients.length >= 2) {
            numb++;
        }
    });

    socket.on('chat message', function (data) {
        // emit to that sockets room, now we use that current_room
        console.log(data);
        io.in(socket.current_room).emit('chat message', data);
    });

    socket.on('disconnect', function () {
        console.log('socket disconnected');
    });
        //new message get
        socket.on('postMsg', function (msg, color, stranger) {
            socket.broadcast.emit('newMsg', socket.nickname, msg, color, stranger);
        });
});

//});


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})
*/

//demp4 
/*var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const config = require('./config');
var dt = new Date();
var utcDate = dt.toUTCString();


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('chat message', function(msg){
      io.emit('chat message', msg);
      console.log(msg)
    });
  });

  socket.on('join',function(user){
    socket.username=user;
   socket.emit('welcome message', socket.username + ", Welcome to the Chat Room");

    var d = new Date();
    var n = d.toLocaleTimeString();
  });

  // for handling real time chatting
    socket.on('chat message', function(msg){
     var d = new Date();
    var n = d.toLocaleTimeString();
     io.emit('chat message', '<div id="user">' + socket.username+'</div><div id="mesg">'+msg+'</div><div id="times">'+n +'</div>');
    });




http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})
*/
/*
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var dt = new Date();
var utcDate = dt.toUTCString();
//var sqlite3 = require('sqlite3').verbose();
//var db = new sqlite3.Database('chat');

// variable declaration done and giving index file
app.get('/', function (req, res) {
    res.sendFile('index.html', { root: __dirname });
});


app.get('/log_out.html', function (req, res) {
    res.sendFile('log_out.html', { root: __dirname });
});

// Session starts
io.on('connection', function (socket) {
    socket.on('join', function (user) {
        socket.username = user;
        socket.emit('welcome message', socket.username + ", Welcome to the Chat Room");

        /*  db.serialize(function() {
          db.run("CREATE TABLE if not exists user (name TEXT, login TEXT)");
          db.run("CREATE TABLE if not exists msglog (name TEXT, msg TEXT,time TEXT)");
          var stmt = db.prepare("INSERT INTO user VALUES (?,?)");*/
  //      var d = new Date();
    //    var n = d.toLocaleTimeString();
        //stmt.run(socket.username, n);
        //stmt.finalize();

        // showing history
/* db.each("SELECT * FROM(SELECT * FROM msglog ORDER BY time DESC limit 10) tmp ORDER by tmp.time ASC;", function(err, row) {
    socket.emit('chat message', '<div id="user">' + row.name+'</div><div id="mesg">'+row.msg+'</div><div id="times">'+row.time +'</div>');
 });*/

/* });

 socket.on('chat message', function (msg) {
     var d = new Date();
     var n = d.toLocaleTimeString();
     io.emit('chat message', '<div id="user">' + socket.username + '</div><div id="mesg">' + msg + '</div><div id="times">' + n + '</div>');
     /*var stmt = db.prepare("INSERT INTO msglog VALUES (?,?,?)");
     stmt.run(socket.username,msg, utcDate);
     stmt.finalize();*/
        // });

  //  });

//});

// for handling real time chatting

//http.listen(3000, function () {
  //  console.log('listening on *:3000');
//});

//demo 3
/*
var app = require("express")();
var http = require("http").Server(app);
var p2pserver = require('socket.io-p2p-server').http
var io = require("socket.io")(http);

const config = require('./config');

var nicknames=[];
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");

});
io.sockets.on("connection", function (socket) {
    socket.on('new user',function(data,callback){
        if(nicknames.indexOf(data)!= -1){
            callback(false);
        }
        else{
            callback(true);
            socket.nickname=data;
            nicknames.push(socket.nickname);
            updateNicknames();
        }
    });
    function updateNicknames(){
        io.sockets.emit('usernames',nicknames);
    }
    socket.on('send message',function(data){
        io.sockets.emit('new message',{msg:data,nick:socket.nickname});
    })

    socket.on('disconnect',function(data){
        if(!socket.nickname) return;
        nicknames.splice(nicknames.indexOf(socket.nickname),1);
        updateNicknames();
    })
});
http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})


*/
//demo 2(omegle)
/*var app = require("express")();
var http = require("http").Server(app);
var p2pserver = require('socket.io-p2p-server').http
var io = require("socket.io")(http);
var Omegle = require('omegle-node');
var om = new Omegle();
const config = require('./config');
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");

});
om.on("connection", function (socket) {

    om.on('omerror', function (err) {
        console.log('error: ' + err);
    });

    //gotID is emitted when you're connected to Omegle
    om.on('gotID', function (id) {
        console.log('connected to the server as: ' + id);
    });

    //waiting is emitted when you're waiting to connect to a stranger
    om.on('waiting', function () {
        console.log('waiting for a stranger.');
    });

    //emitted when you're connected to a stranger
    om.on('connected', function () {
        console.log('connected');
    });

    //emitted when you get a message
    om.on('gotMessage', function (msg) {
        console.log('Stranger: ' + msg);
        om.send('Hi'); //used to send a message to the stranger
    });


    //emitted when the stranger disconnects
    om.on('strangerDisconnected', function () {
        console.log('stranger disconnected.');
    });

})

http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})

//demo1
/*
var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var p2pserver = require('socket.io-p2p-server').http
const config = require('./config');
/*
var queue = [];
var rooms = {};
var allUsers = {};
var users = [];


app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");

});
var findPeerForLoneSocket = function (socket) {
    if (queue) {
        var peer = queue.pop();
        console.log('peer='+peer);
       // console.log('peer_id='+peer.id);
        var room = socket.id;
        console.log('room='+room);
        socket.join(room);
        console.log('socket='+socket);
        //rooms[peer.id] = room;
        rooms[socket.id] = room;
        //peer.emit('chat start', { 'name': names[socket.id], 'room': room });
        socket.emit('chat start', {'room': room });
    } else {
        queue.push(socket);
    }
}


io.sockets.on("connection", function (socket) {
    console.log('user ' + socket.id + ' connected');
    console.log(socket);
    users.push(socket.id);
    console.log(users);

    let len = users.length;

    allUsers[socket.id] = socket;
    findPeerForLoneSocket(socket);

    socket.on("chat message", function (data) {
        var room = rooms[socket.id];
        // io.to(room).emit("chat message", data);
        socket.broadcast.to(room).emit("chat message", data);
    });
    console.log("New user connected " + len),

        socket.on("disconnect", function () {
            users.splice(users.indexOf(socket), 1);
            console.log("User disconnected " + users.length);
        })
});
*//*var queue = [];
var rooms = {};
var names = {};
var allUsers = {};

var findPeerForLoneSocket = function(socket) {

    if (queue) {

        var peer = queue.pop();
        var room = socket.id + '#' + peer.id;
        peer.join(room);
        socket.join(room);
        rooms[peer.id] = room;
        rooms[socket.id] = room;

        peer.emit('chat start', {'name': names[socket.id], 'room':room});
        socket.emit('chat start', {'name': names[peer.id], 'room':room});
    } else {

        queue.push(socket);
    }
}

io.on('connection', function (socket) {
    console.log('User '+socket.id + ' connected');
    socket.on('login', function (data) {
        names[socket.id] = data.username;
        allUsers[socket.id] = socket;
        // now check if sb is in queue
        findPeerForLoneSocket(socket);
    });
    socket.on('message', function (data) {
        var room = rooms[socket.id];
        socket.broadcast.to(room).emit('message', data);
                                    });
    socket.on('leave room', function () {
        var room = rooms[socket.id];
        socket.broadcast.to(room).emit('chat end');
        var peerID = room.split('#');
        peerID = peerID[0] === socket.id ? peerID[1] : peerID[0];
        // add both current and peer to the queue
        findPeerForLoneSocket(allUsers[peerID]);
        findPeerForLoneSocket(socket);
    });
    socket.on('disconnect', function () {
        var room = rooms[socket.id];
        socket.broadcast.to(room).emit('chat end');
        var peerID = room.split('#');
        peerID = peerID[0] === socket.id ? peerID[1] : peerID[0];
        // current socket left, add the other one to the queue
        findPeerForLoneSocket(allUsers[peerID]);
    });
});

http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})
*/



//demo
/*
var app = require("express")();
var http = require("http").Server(app);
//var p2pserver = require('socket.io-p2p-server').http
var io = require("socket.io")(http);

const config = require('./config');

var users = [];
var onlineuser = [];
let room={};
let match;

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");

});
/*io.sockets.on('connection', (socket) => {
    console.log('connected');
    socket.on('onConnected', (data) => {
        console.log(data);
        socket.join(data.vehicleId);
    });
    socket.on('getLocation', (data) => {
        console.log(data);
        io.sockets.in(data.vehicleId).emit('driverLocation', data);

        if (data.isDisconnected) {
            socket.on('disconnect', (data, ack) => {
                ack("disconnected");
            });
        }
    });
});

io.sockets.on("connection", function (socket) {
    console.log('socket id=' + socket.id);
    users.push(socket.id);
    console.log(users);

    let len = users.length;
    room = users[0] + "," + users[1];
    console.log(room);
    //for (var i = 0; i < len - 1; i++) {
     //   console.log(i);
       // room.push(demo[0]+demo[1]);
        //console.log(room);
        //room.push(len[1]);
        //console.log(room);

//    }
    console.log('room='+room)
    //socket.join(room);
    socket.on("chat message", function (data) {
        //  io.emit('chat message',data);
        console.log('join', data.room);
        //socket.join(room, function () {
          //  console.log(room);
           // console.log(socket.rooms);
       // })

        socket.join(data.room)
        io.to(room).emit("chat message", data);
    });
    console.log("New user connected " + len),

        socket.on("disconnect", function () {
            users.splice(users.indexOf(socket), 1);
            onlineuser.splice(onlineuser.indexOf(socket.username), 1);
            console.log("User disconnected " + users.length);
        })
});

http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})
*/


//ios
/*var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const config = require('./config');


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    //  console.log('a user connected');
    console.log('a user connected with the socket id=' + socket.id + "!");
    //   io.broadcast.emit('user connected');
    socket.on('chat message', function (msg) {
        io.emit('chat message', msg);
        console.log(msg)
    });


});


http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})
*/
//with users name

/*var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);
const config = require('./config');
var users =[];
var onlineuser =[];

app.get("/",function(req,res){
	res.sendFile(__dirname+"/index.html");

});
io.sockets.on("connection",function(socket){
	users.push(socket);
	console.log("New user connected "+users.length),

	socket.on("disconnect",function(){
		users.splice(users.indexOf(socket),1);
		onlineuser.splice(onlineuser.indexOf(socket.username),1);
		console.log("User disconnected "+users.length);
	});

	socket.on("new user",function(data){
		socket.username = data;
		onlineuser.push(socket.username);
		console.log("user conected "+socket.username);
		updateuser();
	});

	socket.on("msg",function(name,msg){
		io.sockets.emit("rmsg",{name:name,msg:msg});
	});

	function updateuser(){
		io.sockets.emit("get user",onlineuser);
	}

});


http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})
*/

//normal chat app

/*var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const config = require('./config');

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('chat message', function(msg){
      io.emit('chat message', msg);
      console.log(msg)
    });
  });


http.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})
*/

//with inderjeet sir
/*var express = require('express');
var app = express();
var server = require('http').createServer(app);
var socketio = require('socket.io');
var io = socketio().listen(server);
const config = require('./config');



app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    //console.log('a user connected with the socket id=' + socket.id + "!");

    socket.on('send', function (data) {

        console.log(data + " : has joined the chat ");
       var obj = JSON.parse(data);
       console.log(obj.data);
       console.log(obj.message);
        io.emit(obj.to,obj.data);
    })

    socket.on('disconnected', function () {
        console.log('the user is disconnected');
    });

    /*socket.on('Message', function (data) {
        console.log(data.message);
        io.emit('Message', data);
        console.log(data)
    });*/
/*});

server.listen(config.port, () => {
    console.log('server is start @ ', config.port)
})*/